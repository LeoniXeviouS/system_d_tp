# system_d_tp

## Configuration

VM : Fedora Server 31
* docker-ce-3:19.03.5-3.fc31

# I. systemd-basics

## First steps

### 1.Vérifier que la version de systemd est > 241
 
```
[root@newName fserv]# systemctl --version
systemd 243 (v243.4-1.fc31)
```

### 2. 🌞 S'assurer que systemd est PID1

```
[root@localhost fserv]# ps -ef | grep systemd
root           1       0  0 16:33 ?        00:00:01 /usr/lib/systemd/systemd --switched-root --system --deserialize 30
root         533       1  0 16:33 ?        00:00:00 /usr/lib/systemd/systemd-journald
root         548       1  0 16:33 ?        00:00:00 /usr/lib/systemd/systemd-udevd
root         650       1  0 16:33 ?        00:00:00 /usr/lib/systemd/systemd-logind
fserv        778       1  0 16:35 ?        00:00:00 /usr/lib/systemd/systemd --user
root         897     846  0 16:38 pts/0    00:00:00 grep --color=auto systemd
```

### 3. 🌞 Check tous les autres processus système (**PAS les processus kernel)
```
[root@localhost fserv]# ps -ef | sed -n "/[/,/]/p"
root           1       0  0 16:33 ?        00:00:01 /usr/lib/systemd/systemd --switched-root --system --deserialize 30
root           6       2  0 16:33 ?        00:00:00 [kworker/0:0H-kblockd]
root          10       2  0 16:33 ?        00:00:00 [ksoftirqd/0]
root          12       2  0 16:33 ?        00:00:00 [migration/0]
root          13       2  0 16:33 ?        00:00:00 [cpuhp/0]
root          24       2  0 16:33 ?        00:00:00 [kworker/u2:1-flush-8:0]
root         137       2  0 16:33 ?        00:00:00 [kworker/u3:0]
root         149       2  0 16:33 ?        00:00:00 [kworker/u2:3-events_unbound]
root         234       2  0 16:33 ?        00:00:00 [kworker/0:1H-kblockd]
root         235       2  0 16:33 ?        00:00:00 [kworker/0:3-ata_sff]
root         464       2  0 16:33 ?        00:00:00 [xfs-buf/dm-0]
root         465       2  0 16:33 ?        00:00:00 [xfs-conv/dm-0]
root         466       2  0 16:33 ?        00:00:00 [xfs-cil/dm-0]
root         467       2  0 16:33 ?        00:00:00 [xfs-reclaim/dm-]
root         468       2  0 16:33 ?        00:00:00 [xfs-eofblocks/d]
root         469       2  0 16:33 ?        00:00:00 [xfs-log/dm-0]
root         470       2  0 16:33 ?        00:00:00 [xfsaild/dm-0]
```


## 2. Gestion du temps

### 🌞 Déterminer la différence entre Local Time, Universal Time et RTC time

```
* Local time : Le fuseau horaire de la localisation de la machine. Pour nous Europe/Paris soit (UTC+1) 

* Universal time : C'est l'heure universelle de base. (UTC) 

* RTC time : L'heure de la machine .

- Le RTC Time permet d'avoir une heure qui ne changera jamais (heure d'été/hiver). Une heure très pratique pour synchroniser des serveurs dans des régions géographique différentes.
```

### 🌞 Changer de timezone pour un autre fuseau horaire européen

```
[root@localhost fserv]# timedatectl set-timezone Europe/Andorra
[root@localhost fserv]# timedatectl 
                      Local time: mer. 2019-12-11 21:05:37 CET
                  Universal time: mer. 2019-12-11 20:05:37 UTC
                        RTC time: mer. 2019-12-11 20:05:37
                       Time zone: Europe/Andorra (CET, +0100)
       System clock synchronized: yes
systemd-timesyncd.service active: yes
                 RTC in local TZ: no

```

### 🌞 Désactiver le service lié à la synchronisation du temps avec cette commande, et vérifier à la main qu'il a été coupé

```
[root@localhost fserv]# timedatectl set-timezone Europe/Andorra
[root@localhost fserv]# timedatectl
                      Local time: mer. 2019-12-11 21:06:55 CET
                  Universal time: mer. 2019-12-11 20:06:55 UTC
                        RTC time: mer. 2019-12-11 20:06:55
                       Time zone: Europe/Andorra (CET, +0100)
       System clock synchronized: yes
systemd-timesyncd.service active: inactive
                 RTC in local TZ: no
```

## 3. Gestion de noms

### Changer les noms de la machine

```
[root@localhost fserv]# hostnamectl
   Static hostname: test
         Icon name: computer-vm
           Chassis: vm
        Machine ID: fadf4961b36d4880a15bcaaa52b39629
           Boot ID: aba80027bbe041008dc9ac0163d96038
    Virtualization: oracle
  Operating System: Fedora 31 (Thirty One)
       CPE OS Name: cpe:/o:fedoraproject:fedora:31
            Kernel: Linux 5.3.12-300.fc31.x86_64
      Architecture: x86-64
[root@localhost fserv]# hostnamectl set-hostname newName
[root@localhost fserv]# hostnamectl
   Static hostname: newName
         Icon name: computer-vm
           Chassis: vm
        Machine ID: fadf4961b36d4880a15bcaaa52b39629
           Boot ID: aba80027bbe041008dc9ac0163d96038
    Virtualization: oracle
  Operating System: Fedora 31 (Thirty One)
       CPE OS Name: cpe:/o:fedoraproject:fedora:31
            Kernel: Linux 5.3.12-300.fc31.x86_64
      Architecture: x86-64
```

### 🌞 Expliquer la différence entre les trois types de noms
NOTE : 
```
--static, --transient, --pretty

    If status is invoked (or no explicit command is given) and one of these switches is specified, hostnamectl will print out just this selected hostname.

    If used with set-hostname, only the selected hostname(s) will be updated. When more than one of these switches are specified, all the specified hostnames will be updated. 
```

## 4. Gestion du réseau (et résolution de noms)

### Lister les interfaces et des informations liées

nmcli con show 

```
[root@newName fserv]# nmcli con show
NAME                UUID                                  TYPE      DEVICE 
enp0s3              7ce5d1a3-0b4b-3aad-a601-************  ethernet  enp0s3 
Wired connection 1  8de0023f-42a1-33c5-8b64-************  ethernet  enp0s8 
```

nmcli con show <INTERFACE>

```
[root@newName fserv]# nmcli con show enp0s3
connection.id:                          enp0s3
connection.uuid:                        7ce5d1a3-0b4b-3aad-a601-************
connection.stable-id:                   --
connection.type:                        802-3-ethernet
connection.interface-name:              enp0s3
connection.autoconnect:                 yes
connection.autoconnect-priority:        -999
connection.autoconnect-retries:         -1 (default)
connection.multi-connect:               0 (default)
connection.auth-retries:                -1
```

### Systemd-resolved

#### 🌞 1.Activer la résolution de noms par systemd-resolved

```
systemctl enable systemd-resolved
systemctl status systemd-resolved
● systemd-resolved.service - Network Name Resolution
   Loaded: loaded (/usr/lib/systemd/system/systemd-resolved.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2019-12-05 15:15:57 CET; 6min ago
     Docs: man:systemd-resolved.service(8)
           https://www.freedesktop.org/wiki/Software/systemd/resolved
           https://www.freedesktop.org/wiki/Software/systemd/writing-network-configuration-managers
           https://www.freedesktop.org/wiki/Software/systemd/writing-resolver-clients
 Main PID: 1421 (systemd-resolve)
   Status: "Processing requests..."
    Tasks: 1 (limit: 4689)
   Memory: 4.0M
      CPU: 276ms
   CGroup: /system.slice/systemd-resolved.service
           └─1421 /usr/lib/systemd/systemd-resolved
```

#### 🌞 2.Vérifier qu'un serveur DNS tourne localement et écoute sur un port de l'interfce localhost

```
systemd-resolve --status
Global
       LLMNR setting: yes
MulticastDNS setting: yes
  DNSOverTLS setting: no
      DNSSEC setting: allow-downgrade
    DNSSEC supported: yes
  Current DNS Server: 10.0.2.3
         DNS Servers: 10.0.2.3
                      2a01:cb19:7a2:fd00:2a9e:fcff:fe6f:9ed0
                      192.168.1.1
                      127.0.0.1
```
#### 3. 🌞 Afin d'activer de façon permanente ce serveur DNS, la bonne pratique est de remplacer /etc/resolv.conf par un lien symbolique pointant vers /run/systemd/resolve/stub-resolv.conf

```
[root@localhost fserv]# ln -s /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf
```

#### 4. 🌞 Modifier la configuration de systemd-resolved

```
# /etc/systemd/resolved.conf

[Resolve]
DNS=1.1.1.1
```

##### A. 🌞 Vérifier la modification avec 

```
Global
       LLMNR setting: yes
MulticastDNS setting: yes
  DNSOverTLS setting: no
      DNSSEC setting: allow-downgrade
    DNSSEC supported: yes
         DNS Servers: 1.1.1.1

```

## 5. Gestion de sessions logind

## 6. Gestion d'unité basique (services)

# II. Boot et Logs

## Déterminer le temps qu'a mis sshd.service à démarrer

```
cat graphe.svg |grep "sshd.service"
  <text class="right" x="1797.458" y="4074.000">sshd.service (219ms)</text>

```

# III. Mécanismes manipulés par systemd

## 1. Cgroups

### 🌞 Identifier le cgroup utilisé par votre session SSH

```
systemd-cgls |grep ssh -B 3
│   │   ├─779 /usr/lib/systemd/systemd --user
│   │   └─781 (sd-pam)
│   ├─session-3.scope
│   │ ├─ 815 sshd: fserv [priv]
│   │ ├─ 820 sshd: fserv@pts/0

```

### 🌞Modifier la RAM dédiée à votre session utilisateur

```
[root@newName /]# systemctl set-property session-3.scope MemoryMax=512M


```

## 2. Dbus

busctl: 

```
busctl
NAME                           PID PROCESS         USER            CONNECTION    UNIT                     SESSION DESCRIPTION
:1.1                             1 systemd         root            :1.1          init.scope               -       -          
:1.18                          779 systemd         fserv           :1.18         user@1000.service        -       -          
:1.2                           645 dbus-broker-lau root            :1.2          dbus-broker.service      -       -      
```

le bus système :

```
dbus-monitor --system
signal time=1575559803.232564 sender=org.freedesktop.DBus -> destination=:1.79 serial=4294967295 path=/org/freedesktop/DBus; interface=org.freedesktop.DBus; member=NameAcquired
   string ":1.79"
signal time=1575559803.234429 sender=org.freedesktop.DBus -> destination=:1.79 serial=4294967295 path=/org/freedesktop/DBus; interface=org.freedesktop.DBus; member=NameLost
   string ":1.79"
signal time=1575559747.589654 sender=:1.1 -> destination=(null destination) serial=6234 path=/org/freedesktop/systemd1; interface=org.freedesktop.systemd1.Manager; member=UnitNew
   string "syslog.service"
   object path "/org/freedesktop/systemd1/unit/syslog_2eservice"
signal time=1575559747.589826 sender=:1.1 -> destination=(null destination) serial=6235 path=/org/freedesktop/systemd1; interface=org.freedesktop.systemd1.Manager; member=UnitNew
   string "syslog.socket"
   object path "/org/freedesktop/systemd1/unit/syslog_2esocket"
signal time=1575559747.590014 sender=:1.1 -> destination=(null destination) serial=6236 path=/org/freedesktop/systemd1; interface=org.freedesktop.systemd1.Manager; member=UnitNew
   string "emergency.service"
   object path "/org/freedesktop/systemd1/unit/emergency_2eservice"
signal time=1575559747.590206 sender=:1.1 -> destination=(null destination) serial=6237 path=/org/freedesktop/systemd1; interface=org.freedesktop.systemd1.Manager; member=UnitNew
   string "emergency.target"
   object path "/org/freedesktop/systemd1/unit/emergency_2etarget"
signal time=1575559747.590388 sender=:1.1 -> destination=(null destination) serial=6238 path=/org/freedesktop/systemd1; interface=org.freedesktop.systemd1.Manager; member=UnitNew
   string "local-fs.target"
   object path "/org/freedesktop/systemd1/unit/local_2dfs_2etarget"
signal time=1575559747.590575 sender=:1.1 -> destination=(null destination) serial=6239 path=/org/freedesktop/systemd1; interface=org.freedesktop.systemd1.Manager; member=UnitNew

```

### 🌞 Observer, identifier, et expliquer complètement un évènement choisi

```
busctl monitor
Monitoring bus message stream.
‣ Type=method_call  Endian=l  Flags=0  Version=1  Priority=0 Cookie=1
  Sender=:1.117  Destination=org.freedesktop.DBus  Path=/org/freedesktop/DBus  Interface=org.freedesktop.DBus  Member=Hello
  UniqueName=:1.117
  MESSAGE "" {
  };

‣ Type=method_return  Endian=l  Flags=1  Version=1  Priority=0 Cookie=-1  ReplyCookie=1
  Sender=org.freedesktop.DBus  Destination=:1.117
  MESSAGE "s" {
          STRING ":1.117";
  };

‣ Type=signal  Endian=l  Flags=1  Version=1  Priority=0 Cookie=-1
  Sender=org.freedesktop.DBus  Path=/org/freedesktop/DBus  Interface=org.freedesktop.DBus  Member=NameOwnerChanged
  MESSAGE "sss" {
          STRING ":1.117";
          STRING "";
          STRING ":1.117";
  };

‣ Type=signal  Endian=l  Flags=1  Version=1  Priority=0 Cookie=-1
  Sender=org.freedesktop.DBus  Destination=:1.117  Path=/org/freedesktop/DBus  Interface=org.freedesktop.DBus  Member=NameAcquired
  MESSAGE "s" {
          STRING ":1.117";
  };

‣ Type=method_call  Endian=l  Flags=0  Version=1  Priority=0 Cookie=2
  Sender=:1.117  Destination=org.freedesktop.systemd1  Path=/org/freedesktop/systemd1  Interface=org.freedesktop.systemd1.Manager  Member=GetDynamicUsers
  UniqueName=:1.117
  MESSAGE "" {
  };
```

## 3. Restriction et isolation

Le service est créé : 

```
systemctl status run-u125
● run-u125.service - /bin/bash
   Loaded: loaded (/run/systemd/transient/run-u125.service; transient)
Transient: yes
   Active: active (running) since Thu 2019-12-05 19:00:34 CET; 2min 41s ago
 Main PID: 2821 (bash)
    Tasks: 3 (limit: 4689)
   Memory: 3.7M
      CPU: 237ms
   CGroup: /system.slice/run-u125.service
           ├─2821 /bin/bash
           ├─2853 systemctl status run-u125
           └─2854 less

```

Le cgroup est créé : 

```

Dec 05 19:00:34 newName systemd[1]: Started /bin/bash.
[root@newName /]# systemd-cgls | grep run-u125 -B 24
└─system.slice
  ├─systemd-udevd.service
  │ └─548 /usr/lib/systemd/systemd-udevd
  ├─dbus-broker.service
  │ ├─645 /usr/bin/dbus-broker-launch --scope system --audit
  │ └─646 dbus-broker --log 4 --controller 9 --machine-id fadf4961b36d4880a15bc…
  ├─chronyd.service
  │ └─641 /usr/sbin/chronyd
  ├─auditd.service
  │ └─619 /sbin/auditd
  ├─systemd-journald.service
  │ └─533 /usr/lib/systemd/systemd-journald
  ├─sshd.service
  │ └─691 /usr/sbin/sshd -D -oCiphers=aes256-gcm@openssh.com,chacha20-poly1305@…
  ├─NetworkManager.service
  │ └─2689 /usr/sbin/NetworkManager --no-daemon
  ├─firewalld.service
  │ └─638 /usr/bin/python3 /usr/sbin/firewalld --nofork --nopid
  ├─sssd.service
  │ ├─639 /usr/sbin/sssd -i --logger=files
  │ ├─647 /usr/libexec/sssd/sssd_be --domain implicit_files --uid 0 --gid 0 --l…
  │ └─648 /usr/libexec/sssd/sssd_nss --uid 0 --gid 0 --logger=files
  ├─systemd-resolved.service
  │ └─2771 /usr/lib/systemd/systemd-resolved
  ├─run-u125.service
  │ ├─2821 /bin/bash
  │ ├─2855 systemd-cgls
  │ └─2856 grep --color=auto run-u125 -B 24

```

### 🌞 Le cgroup utilisé est : 
* system.slice

### 🌞 Lancer une nouvelle commande et limiter la memoire max : 

```
sudo systemd-run -p MemoryMax=512M --wait -t /bin/bash
Running as unit: run-u131.service
Press ^] three times within 1s to disconnect TTY.
[root@newName /]# systemctl status run-u131
● run-u131.service - /bin/bash
   Loaded: loaded (/run/systemd/transient/run-u131.service; transient)
Transient: yes
   Active: active (running) since Thu 2019-12-05 19:07:08 CET; 31s ago
 Main PID: 2870 (bash)
    Tasks: 3 (limit: 4689)
   Memory: 2.2M (max: 512.0M)
      CPU: 44ms
   CGroup: /system.slice/run-u131.service
           ├─2870 /bin/bash
           ├─2887 systemctl status run-u131
           └─2888 less

```

### 🌞 Lancer une nouvelle commande : ajouer un tracage réseau

```
[root@newName /]# exit
exit
Finished with result: exit-code
Main processes terminated with: code=exited/status=127
Service runtime: 1min 26.650s
CPU time consumed: 60ms
IP traffic received: 416B
IP traffic sent: 416B

```

# IV. Systemd units in-depth

## 1. Exploration de services existants

### Observer l'unité

#### A. Trouver le path où est défini le fichier

```
/etc/systemd/system/auditd.service
```
## 2. Création de service simple

## 3. Sandboxing (heavy security)

## 4. Event-based activation

### A. Activation via socket UNIX 

### B. Activation automatique d'un point de montage

### C. Timer systemd

## 5. Ordonner et grouper des services


